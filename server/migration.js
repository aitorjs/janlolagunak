const excelToJson = require('convert-excel-to-json');
const { MongoClient } = require('mongodb');

const mongo = {
    port: 7000,
    db: "janlolagunak"
}
const url = `mongodb+srv://janlolagunak:STshxcuMCphHDdJH@cluster0.bgywnkb.mongodb.net/?retryWrites=true&w=majority`;

async function main() {
    let necesidadesCounter = 0;
    let donesCounter = 0;

    const result = excelToJson({
        sourceFile: 'migration.ods'
    });
    console.log("result", result)
    delete result.necesidades[0];
    // delete result.dones[0];

    const necesidades = result.necesidades;
    // const dones = result.dones;

    const client = new MongoClient(url);

    await client.connect();
    // console.log('Connected successfully to mongodb');
    const db = client.db(mongo.db);

    // TODO: cambiar nombre de coleccion a necesidades
    const necesidadesDB = db.collection('ofrecimientos');
    // const donesDB = db.collection('dones');

    // TODO: Buscar todas las comunidades dentro del calc y rellenar su coleccion

    const a = necesidades.map(async necesidad => {
        // TODO: trim y rtrim para quitar los espacios delante y detras
        // TODO: entran en minusculas y sin acentos.
        let categories = necesidad["B"];

        // TODO: Buscar si alguien tiene una necesidad con ese mismo nombre es todo
        // TODO2: Buscar si alguien tiene un don con ese mismo nombre es done
        // TODO3: Cambiar todo y done en cada una necesidad o don.

        // TODO: trim y rtrim para quitar los espacios delante y detras
        // TODO: entran en minusculas y sin acentos.
        categories = categories.split(",");

        // TODO: Gestion de datos de usuario: pseudonimo, telegram?, email?, telefono?

        const necesidadesDoc = {
            name: necesidad["A"],
            todo: 1, // necesidades
            done: 1, // dones
            categories,
            location: {
                type: "Point",
                coordinates: [
                    parseFloat(necesidad["C"]),
                    parseFloat(necesidad["D"])
                ]
            },
            marker: "",
            community: necesidad["E"]
        };
        // console.log("dato", ofrecimientoDoc)
        necesidadesCounter++;
        return necesidadesDB.insertOne(necesidadesDoc);
    })
    await Promise.all(a);
    await client.close();
    console.log(`${necesidadesCounter} necesidades añadidas.`)
}

main();
