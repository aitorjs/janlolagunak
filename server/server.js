const { MongoClient } = require('mongodb');
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');

const urlencodedParser = bodyParser.urlencoded({ extended: false })
const mongo = {
  port: 7000,
  db: "janlolagunak"
}
const url = `mongodb+srv://janlolagunak:STshxcuMCphHDdJH@cluster0.bgywnkb.mongodb.net/?retryWrites=true&w=majority`;

// node_modules/convert-excel-to-json/bin/cli.js  --sourceFile migration.ods

app.use(cors());

/* app.use(express.static('public'));
app.get('/index.html', function (req, res) {
  res.sendFile(__dirname + "/" + "index.html");
}) */

app.post('/search', urlencodedParser, async (req, res) => {
  const client = new MongoClient(url);
  console.log("REQ", req.body)
  const type = req.body["type"];
  let categories = req.body["categories[]"];
  let community = req.body["community[]"];
  let freeText = req.body["$text[$search]"];
  let maxDistance = req.body["location[$maxDistance]"];
  let search = {};
  /* let search = {
    categories: { $in: categories },
    community: { $in: community },
    $text: { $search: `\"${freeText}\"` } } 
    // $text: { $search: "\"casero\"" }
    location: {
      $near: {
        $geometry: { type: "Point", coordinates: [43.3418969, -1.8073705] },
        $minDistance: 0,
        $maxDistance: 1000
      }
    }
  } */


  console.log('ASD', typeof community);
  if (community != null) {
    search.community = {};

    if (typeof community === "string") {
      search.community = { $in: [community] };
    } else {
      search.community = { $in: community };
    }
  }

  if (categories != null) {
    search.categories = {};

    if (typeof categories === "string") {
      search.categories = { $in: [categories] };
    } else {
      search.categories = { $in: categories };
    }
  }

  // console.log("`\"casero\"`", `\`\"${freeText}\"\``, freeText)
  if (freeText != undefined && freeText != "") {
    console.log("hace busqueda")
    search["$text"] = {};
    search["$text"] = { $search: `\`\"${freeText}\"\`` };
    hasFreeTextFilled = true;
  }

  if (maxDistance != undefined && parseInt(maxDistance) !== 0) {
    console.log("entra en distance")

    // delete search["community"];

    // si tiene freeText:
    if (freeText != undefined && freeText != "") {
      console.log("tiene free text. do nothing")

      delete search["$text"];
    }

    const geometry = req.body["location[$geometry][]"];

    search["location"] = {};
    search["location"]["$near"] = {};
    search["location"]["$near"]["$geometry"] = { type: "Point", coordinates: [parseFloat(geometry[0]), parseFloat(geometry[1])] };
    search["location"]["$near"]["$minDistance"] = parseInt(0);
    search["location"]["$near"]["$maxDistance"] = parseInt(req.body["location[$maxDistance]"]);
    // hasFreeTextFilled = true;

  }
  console.log('SEARCH', search);

  await client.connect();
  console.log('Connected successfully to mongodb');
  const db = client.db(mongo.db);

  // const categories = db.collection('categories');
  const necesidades = db.collection(type);
  // const response = await categories.find({}).toArray();
  let response = await necesidades.find(search).toArray();
  client.close();

  if (maxDistance != undefined && parseInt(maxDistance) !== 0 && freeText != undefined && freeText != "") {
    console.log("freeText with distance", freeText, response)
    response = response.filter((res) => {
      return res.name.startsWith(freeText) || res.name.includes(freeText)
    });
  }
  console.log("first category", response)

  res.end(JSON.stringify(response));
})

/* Formas de hacer los selects
// CATEGORYES AND COMUNNITY
db.getCollection('categories').find({name: 'jan'});
db.getCollection('necesidades').find({ 
  categories: { $in: [ "jan" ] }, 
  community: { $in: [ "irun" ] } 
})

// TEXT SEARCH https://www.mongodb.com/docs/manual/core/link-text-indexes/
// 1. Create index
// db.getCollection('necesidades').createIndex( { name: "text" } )
// 2. Make search
// db.getCollection('necesidades').find( { $text: { $search: "\"perretes\"" } })
// db.getCollection('necesidades').find({ 
  categories: { $in: [ "jan" ] }, 
  community: { $in: [ "irun" ] },
  $text: { $search: "\"casero\"" }
})

{ 
  "name": 'Pasear a mis perretes por el barrio. Pasear a mis perretes por el b',
  "todo": 333,
  "done": 111,
  "categories": ['jan', 'lagunak'],
  "location": { type: "Point", coordinates: [ 43.3418969, -1.8073705 ] },
  "marker": '',
  "community": "irun"
}

// GEOSPACIAL QUERYS https://www.mongodb.com/docs/manual/geospatial-queries/#examples
// 0. Add point column (location) to all documents
// location: { type: "Point", coordinates: [ 43.3418969, -1.8073705 ] },
// 1. Create index
// db.getCollection('necesidades').createIndex( { location: "2dsphere" } )
// 2. Make search
// db.getCollection('necesidades').find(
  {
     location:
     { $near:
      {
            $geometry: { type: "Point",  coordinates: [ 43.3418969, -1.8073705 ] },
            $minDistance: 1000,
            $maxDistance: 5000
          }
        },
        categories: { $in: [ "jan" ] }, 
        community: { $in: [ "irun" ] }
      })
      // db.getCollection('necesidades').find(
   {
     location:
       { $near:
        {
          $geometry: { type: "Point",  coordinates: [ 43.3418969, -1.8073705 ] },
          $minDistance: 1000,
          $maxDistance: 5000
        }
      }
    })
*/

/* const url = `mongodb+srv://janlolagunak:STshxcuMCphHDdJH@cluster0.bgywnkb.mongodb.net/?retryWrites=true&w=majority`;
const client = new MongoClient(url);
 
async function main() {
  const dbName = 'janlolagunak';
 
  await client.connect();
  console.log('Connected successfully to server');
  const db = client.db(dbName);
 
  // const categories = db.collection('categories');
  const necesidades = db.collection('necesidades');
  const search = {
    categories: { $in: ["jan"] },
    community: { $in: ["irun"] },
    $text: { $search: "\"casero\"" }
  }
  // const res = await categories.find({}).toArray();
  const res = await necesidades.find(search).toArray();
 
  console.log("first category", res)
  return true;
}
 
main()
  .then()
  .catch(console.error)
  .finally(() => client.close()); */

app.listen(process.env.PORT || mongo.port);
